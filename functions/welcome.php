<?php
/**
 * Admin Welcome Screen - Do not delete!
 *
 * @package WordPress
 * @subpackage Elegant WPExplorer Theme
 * @since Elegant 1.0
 */



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * WPEX_Welcome Class
 *
 * A general class for About and Credits page.
 *
 * @since 1.4
 */
class WPEX_Welcome {

	/**
	 * @var string The capability users should have to view the page
	 */
	public $minimum_capability = 'manage_options';

	/**
	 * Get things started
	 *
	 * @since 1.0
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menus') );
		add_action( 'admin_head', array( $this, 'admin_head' ) );
		add_action( 'admin_init', array( $this, 'welcome' ) );
	}

	/**
	 * Register the Dashboard Pages which are later hidden but these pages
	 * are used to render the Welcome and Credits pages.
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function admin_menus() {
		// About
		add_dashboard_page(
			__( 'Theme Details', 'wpex' ),
			__( 'Theme Details', 'wpex' ),
			$this->minimum_capability,
			'wpex-about',
			array( $this, 'about_screen' )
		);

		// Recommended
		add_dashboard_page(
			__( 'Recommendations | Elegant Theme', 'wpex' ),
			__( 'Recommendations', 'wpex' ),
			$this->minimum_capability,
			'wpex-recommended',
			array( $this, 'recommended_screen' )
		);

	}

	/**
	 * Hide dashboard tabs from the menu
	 *
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function admin_head() {
		remove_submenu_page( 'index.php', 'wpex-recommended' );
	}

	/**
	 * Navigation tabs
	 *
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function tabs() {
		$selected = isset( $_GET['page'] ) ? $_GET['page'] : 'wpex-about'; ?>
		<h2 class="nav-tab-wrapper">
			<a class="nav-tab <?php echo $selected == 'wpex-about' ? 'nav-tab-active' : ''; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'wpex-about' ), 'index.php' ) ) ); ?>">
				<?php _e( "About", 'wpex' ); ?>
			</a>
			<a class="nav-tab <?php echo $selected == 'wpex-recommended' ? 'nav-tab-active' : ''; ?>" href="<?php echo esc_url( admin_url( add_query_arg( array( 'page' => 'wpex-recommended' ), 'index.php' ) ) ); ?>">
				<?php _e( 'Recommended', 'wpex' ); ?>
			</a>
		</h2>
		<?php
	}

	/**
	 * Render About Screen
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function about_screen() { ?>
		<div class="wrap about-wrap">
			Hello
			</div>
		<?php
	}

	/**
	 * Render Recommended Screen
	 *
	 * @access public
	 * @since 1.4
	 * @return void
	 */
	public function recommended_screen() { ?>
		<div class="wrap about-wrap">

			World
		</div>
		<?php
	}

	/**
	 * Sends user to the Welcome page on theme activation
	 *
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function welcome() {
		global $pagenow;
		if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
			return;
		}
		if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
			wp_safe_redirect( admin_url( 'admin.php?page=wpex-about' ) ); exit;
			exit;
		}
	}
}
new WPEX_Welcome();